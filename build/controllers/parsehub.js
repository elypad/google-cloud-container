"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const axios_1 = __importDefault(require("axios"));
const cors = require('cors')({ origin: true });
const getProjects = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    let result = yield axios_1.default.get(`https://parsehub.com/api/v2/projects/${req.params.projectToken}?api_key=${req.params.apiKey}`);
    let post = result.data;
    return res.status(200).json(post);
});
const getLastReadyData = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    cors(req, res, () => __awaiter(void 0, void 0, void 0, function* () {
        let result = yield axios_1.default.get(`https://parsehub.com/api/v2/projects/${req.params.projectToken}/last_ready_run/data?api_key=${req.params.apiKey}&format=json`);
        let post = result.data;
        let cleanCars = getObjects(post, 'price', '');
        console.log("ALL CARS ARE: ", cleanCars.length);
        cleanCars.forEach(eachCar => {
            console.log(eachCar.carTitle);
        });
        return res.status(200).json(cleanCars);
    }));
});
const getDataForRun = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    cors(req, res, () => { });
    let result = yield axios_1.default.get(`https://parsehub.com/api/v2/runs/${req.params.runToken}/data?api_key=${req.params.apiKey}&format=json`);
    let post = result.data;
    return res.status(200).json(post);
});
//// GET OBJECTS WITH MAKE FROM JSON FILE ////////
function getObjects(obj, key, val) {
    //  console.log("RECEIVED", obj.length )
    let objects = [];
    for (var i in obj) {
        if (!obj.hasOwnProperty(i))
            continue;
        if (typeof obj[i] == 'object') {
            objects = objects.concat(getObjects(obj[i], key, val));
        }
        else 
        //if key matches and value matches or if key matches and value is not passed (eliminating the case where key matches but passed value does not)
        if (i == key && obj[i] == val || i == key && val == '') { //
            objects.push(obj);
        }
        else if (obj[i] == val && key == '') {
            //only add if the object is not already in the array
            if (objects.lastIndexOf(obj) == -1) {
                objects.push(obj);
            }
        }
    }
    return objects;
}
exports.default = { getProjects, getLastReadyData, getDataForRun };
