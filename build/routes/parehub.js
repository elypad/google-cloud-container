"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
/** source/routes/posts.ts */
const express_1 = __importDefault(require("express"));
const parsehub_1 = __importDefault(require("../controllers/parsehub"));
const router = express_1.default.Router();
router.get('/projects/:apiKey/:projectToken', parsehub_1.default.getProjects);
router.get('/last-ready-data/:apiKey/:projectToken', parsehub_1.default.getLastReadyData);
router.get('/last-ready-data/:apiKey/:runToken', parsehub_1.default.getDataForRun);
module.exports = router;
