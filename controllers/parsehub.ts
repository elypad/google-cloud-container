/** source/controllers/posts.ts */
import { Request, Response, NextFunction } from 'express';
import axios, { AxiosResponse } from 'axios';
/* eslint-disable */
// import * as functions from "firebase-functions";
import * as admin from "firebase-admin";
// const firebase = require("firebase");
// import { DatePipe } from '@angular/common';

// const datePipe = new DatePipe('en-US');

const cors = require('cors')({origin: true});


let websiteStock: any = []
let newCars: any[] = []
let carsAlreadyExisting: any[] = []
let currentEURprice: number = 0

let duties: number = 30
let channel: string = `Dubai`
let gbpToEurRate: number = 1.19
let kuweitDinnarToGbpRate: number = 2.45
let qatariRialToGbpRate: number = 0.20
let uaeDirhamToGbpRate: number = 0.20
let poundRate: number = 0.21
let euroRate: number = 0.23
let qtrPoundRate: number = 0.20
let qtrEuroRate: number = 0.23
let isDubizzle: boolean = true
let source: string = `Dubizzle`



const environment = {
    production: true,
    firebase: {
      apiKey: "AIzaSyB32PqWU-eCNcqFuUuXFI5XKmFd0E7ntpg",
      authDomain: "direct-from-dubai.firebaseapp.com",
      databaseURL: "https://direct-from-dubai.firebaseio.com",
      projectId: "direct-from-dubai",
      storageBucket: "direct-from-dubai.appspot.com",
      messagingSenderId: "191492841609",
      appId: "1:191492841609:web:0c52e7943fc4b2ac",
    },
    stripeApiKey: "pk_test_2A9qIsc24z965nEycOKO3HVe",
    twilio: {
      accountSid: "AC624cc591d8bb747188f60870386cf434",
      authToken: "679a7409fb7c160bea547e3d131f06bf",
    },
    parsehub: {
      PARSEHUB_API_KEY: "tt0O7QxTTa7H",
    },
  }

admin.initializeApp(environment.firebase);

let adminPress:admin.firestore.QuerySnapshot<admin.firestore.DocumentData>
let stock:admin.firestore.QuerySnapshot<admin.firestore.DocumentData>

fetchAllPrefferences().then( resp => {
    adminPress = resp
    console.log(`${resp.size} Admin Car Prefferences`)
}).catch( err => {
    console.log("Cannot get admin car prefferences",err)
})


allAvailableCars().then( res => { 
    stock = res
    console.log(`${res.size} Available Cars`)
}).catch( err => {
    console.log("Cannot get stock cars",err)
})



const getProjects = async (req: Request, res: Response, next: NextFunction) => {
    let result: AxiosResponse = await axios.get(`https://parsehub.com/api/v2/projects/${req.params.projectToken}?api_key=${req.params.apiKey}`);
    let post = result.data;
     return res.status(200).json(post);
};

const getLastReadyData = async (req: Request, res: Response, next: NextFunction) => {

    cors( req, res, async () => {
        
            let result: AxiosResponse = await axios.get(`https://parsehub.com/api/v2/projects/${req.params.projectToken}/last_ready_run/data?api_key=${req.params.apiKey}&format=json`);
            let post = result.data;
            // let carsWithImages: any[] = getObjects(post,'images','')
            let flatCars:any[] = getObjects(post,'price','')
            let carsInTheFeed: any[] = []

            console.log("Cars In Feed: ",flatCars.length)
        
            flatCars.forEach( eachFlatCar => {
                let hasImages: boolean = eachFlatCar.images === undefined ? false : true

                let myMileage = eachFlatCar.carLink.includes('qatarliving') ? (Math.round(getMileageKmsFromDescription(eachFlatCar.description))) : eachFlatCar.mileage.replace(/,/g, '')
                let unique: string =  eachFlatCar.carLink.includes('dubizzle') ? `${eachFlatCar.year}${eachFlatCar.make}${eachFlatCar.model}${eachFlatCar.color}${Number(eachFlatCar.cylinderCount)}${myMileage}` : eachFlatCar.carLink

                let myNewYear: number = eachFlatCar.carLink.includes('qatarliving') ? lastWord(eachFlatCar.carTitle) : eachFlatCar.carLink.includes('carsemsar') ? firstWord(eachFlatCar.carTitle) : eachFlatCar.year.replace(/[^0-9.-]+/g,"");
                let cleanTitle =  eachFlatCar.carTitle.split('Used QAR')[0]
                let moreCleanTitle = cleanTitle.replace(myNewYear,"")

                let make = eachFlatCar.carLink.includes('dubicars') ? eachFlatCar.make : eachFlatCar.carLink.includes('dubizzle') ? eachFlatCar.make : eachFlatCar.carLink.includes('motorgy') ? generateMake(eachFlatCar.carTitle) : generateMake(moreCleanTitle)
                let model = eachFlatCar.carLink.includes('dubicars') ? (eachFlatCar.model.replace(eachFlatCar.make,"").trim()) : eachFlatCar.carLink.includes('dubizzle') ? eachFlatCar.model : eachFlatCar.carLink.includes('motorgy') ? eachFlatCar.model : generateModel(make,moreCleanTitle) 
                
                let ukPrice = getUkPrice(Number(eachFlatCar.price.replace(/[^0-9.-]+/g,"")), eachFlatCar.carLink)
                let europePrice = currentEURprice 

                const basePrice =  eachFlatCar.carLink.includes('motorgy') ? (Number(eachFlatCar.price.replace(/[^0-9.-]+/g,""))): Number(eachFlatCar.price.replace(/[^0-9.-]+/g,""));

                let updatedCar = {
                    price: eachFlatCar.price ? eachFlatCar.price: 0 ,
                    vin:'',
                    warranty: eachFlatCar.warranty? eachFlatCar.warranty:'',
                    postedDate: eachFlatCar.postedDate ? eachFlatCar.postedDate:'',
                    AED: basePrice ? basePrice : 0 ,
                    historyCheck: eachFlatCar.historyCheck? eachFlatCar.historyCheck:'',
                    GBP: ukPrice ? ukPrice : 0 ,
                    EUR: europePrice ? europePrice : 0 ,
                    regNumber:'',
                    modelYear: Number(myNewYear) ? Number(myNewYear) : 0,
                    make: make.trim().toUpperCase(),
                    model: eachFlatCar.carLink.includes('motorgy') ? (eachFlatCar.carTitle.includes('Mercedes') ? generateClassesForMercedes(eachFlatCar.model): eachFlatCar.model) : model.trim().toUpperCase(),
                    engineModel: eachFlatCar.carLink.includes('motorgy') ? (eachFlatCar.carTitle.includes('Mercedes') ? eachFlatCar.model: '') : (eachFlatCar.engineModel || ''),
                    interiorDecor: eachFlatCar.interiorColor ? eachFlatCar.interiorColor :'',
                    interiorColor: eachFlatCar.interior ? eachFlatCar.interior:'',
                    regionalSpecs: eachFlatCar.regionalSpecs ? eachFlatCar.regionalSpecs:'GCC',
                    transmissionType:  eachFlatCar.transmission ? eachFlatCar.transmission : 'Automatic',
                    body:eachFlatCar.body? eachFlatCar.body:'',
                    description: eachFlatCar.carLink.includes('oasis') ? getQatarExtras(eachFlatCar.extras): eachFlatCar.carLink.includes('dubizzle') ? (eachFlatCar.source ? (eachFlatCar.source.includes('dubizzle pro') ? descriptionForVehicle(res, basePrice) : eachFlatCar.description): eachFlatCar.description ) : (eachFlatCar.description || ""),
                    bodyColor: eachFlatCar.color ? eachFlatCar.color : '',
                    doors: eachFlatCar.doors ?  Number(eachFlatCar.doors.replace(/[^0-9.-]+/g,"")) : '',
                    fuelType: eachFlatCar.fuelType ? eachFlatCar.fuelType : 'Petrol',
                    engineCylinders: eachFlatCar.carLink.includes('dubizzle') ? Number(eachFlatCar.cylinderCount) :  eachFlatCar.carLink.includes('oasis') ? (eachFlatCar.cylinderCount.replace(/[^0-9.-]+/g,"")) : (eachFlatCar.cylinderCount ? eachFlatCar.cylinderCount.replace(/[^0-9.-]+/g,"") : ''),
                    drivetrain: eachFlatCar.drivetrain ? eachFlatCar.drivetrain : '',
                    engineSize: eachFlatCar.engineSize? eachFlatCar.engineSize: '',
                    condition:{
                      description: '',
                      qualificationReport:'',
                      inspectionReport:'',
                    },
                    isStock: false,
                    // price: myPrice,
                    mileageMiles: eachFlatCar.carLink.includes('qatarliving') ? (Math.round(getMileageKmsFromDescription(eachFlatCar.description)*0.621371)): Math.round((eachFlatCar.mileage.replace(/[^0-9.-]+/g,"")*0.621371)),
                    // mileageMiles: eachFlatCar.mileage ? Math.round((eachFlatCar.mileage.replace(/[^0-9.-]+/g,"")*0.621371)) : 0,
                    // mileage: eachFlatCar.mileage ? Number(eachFlatCar.mileage.replace(/[^0-9.-]+/g,"")):0,
                    mileage: eachFlatCar.carLink.includes('qatarliving') ? getMileageKmsFromDescription(eachFlatCar.description): Number(eachFlatCar.mileage.replace(/[^0-9.-]+/g,"")),
                    numberOfOwners:'',
                    images: getImages(eachFlatCar),
                    extras: eachFlatCar.carLink.includes('motorgy') ? (getQatarExtras(eachFlatCar.extras) || []): eachFlatCar.carLink.includes('oasis') ? [] : (eachFlatCar.extras ? eachFlatCar.extras : []),
                    customExtras: eachFlatCar.customExtras  ? eachFlatCar.customExtras : [],
                    carTitle: eachFlatCar.source ? (eachFlatCar.source.includes('dubizzle cars') ? carTitleWithMoreDetails(res) : cleanTitle) : cleanTitle,
                    dubizzleTitle: eachFlatCar.carLink.includes('dubizzle') ? eachFlatCar.carTitle : eachFlatCar.carLink.includes('qatarliving') ? cleanTitle: eachFlatCar.carTitle,
                    carLink: eachFlatCar.carLink,
                  // let  =  eachFlatCar.carTitle.split('Used QAR')[0]
              
                    userId:'HqYwd3wisRbztV9llAQebQOTf0e2',
                    owner: 'Elkana Rop',
                    ownerCategory: 'individual',
                    ownerCompany: 'individual',
                    ownerPhone:'',
                    status:'published',
                    master:true,
                    favourite: false,
                    tag: eachFlatCar.source? eachFlatCar.source:'',
                    // source: eachFlatCar.carLink.includes("qmotor") ? 'qmotors': eachFlatCar.carLink.includes("dubicars") ? 'dubicars': eachFlatCar.carLink.includes("qatarliving") ? 'qliving' : eachFlatCar.carLink.includes("oasiscars") ? 'oasis' : eachFlatCar.carLink.includes("qatarsale") ? 'qatarsale': eachFlatCar.carLink.includes("carsemsar") ? 'carsemsar': eachFlatCar.carLink.includes("motorgy") ? 'motorgy' : 'dubizzle',
                    source: eachFlatCar.carLink.includes("dubicars") ? 'dubicars':eachFlatCar.carLink.includes("qmotor") ? 'qmotors': eachFlatCar.carLink.includes("dubicars") ? 'dubicars': eachFlatCar.carLink.includes("qatarliving") ? 'qatarliving' : eachFlatCar.carLink.includes("oasiscars") ? 'oasis' : eachFlatCar.carLink.includes("qatarsale") ? 'qatarsale': eachFlatCar.carLink.includes("carsemsar") ? 'carsemsar': eachFlatCar.carLink.includes("motorgy") ? 'motorgy' : eachFlatCar.carLink.includes("q84sale") ? 'q84sale': eachFlatCar.carLink.includes("dubizzle") ? (eachFlatCar.source.includes("wner")? 'dubizzle': eachFlatCar.source.includes("dubizzle cars")? 'dubizzle pro': 'dubizzle') : 'dubizzle',
                    version:2,
                    depositeTaken: eachFlatCar.carTitle.toLowerCase().includes('taken') ? true:false,
                    horsePower: eachFlatCar.horsePower? eachFlatCar.horsePower: '',
                    dateStamp: Date.parse(new Date().toUTCString()),
                    stockNumber: randNumber(),
                    createdDate: Date.parse(formatDate(new Date().toUTCString())),
                    qualificationToken: {
                      approved: false,
                      approvedBy: 'HqYwd3wisRbztV9llAQebQOTf0e2',
                      dateTimeApproved: Date.parse(new Date().toUTCString()),
              
                    },
                    // uniqueness:`${Number(eachFlatCar.year)}${eachFlatCar.model.trim().toLowerCase()}${Number(eachFlatCar.cylinderCount)}${Number(eachFlatCar.mileage)}`,
                    uniqueness: eachFlatCar.carLink.includes('dubizzle') ? unique : eachFlatCar.carLink,
                    market: eachFlatCar.carLink.includes('dubizzle') ? 'dubai' : eachFlatCar.carLink.includes('dubicars') ? 'dubai' : 'emirates',
                    hasImages: hasImages

                }


                if(updatedCar.hasImages){

                    console.log(`${updatedCar.carTitle} has IMAGES`)
                    carsInTheFeed.push(updatedCar)
                    checkIfExists(updatedCar)

                }else{

                    console.log(`Skipt ${updatedCar.carTitle} with no IMAGES!`)
                }

                
            })
            
            let todaysCars: any[] = []
            adminPress.forEach(  eachPreff => {
        
                let myResults: any[] = newCars.filter( car=> car.make.replace(/[^a-zA-Z ]/g, "").replace(/\s/g, "").toLowerCase().trim() === eachPreff.get('make').replace(/[^a-zA-Z ]/g, "").replace(/\s/g, "").toLowerCase().trim() && 
                car.carTitle.replace(/[^a-zA-Z ]/g, "").replace(/\s/g, "").toLowerCase().trim().includes(eachPreff.get('model').replace(/[^a-zA-Z ]/g, "").replace(/\s/g, "").toLowerCase().trim()) )
        
                if(myResults.length>0){
                    websiteStock.push(...myResults)
                    // console.log("Perfect match for ", eachPreff.get('make'), eachPreff.get('model'))
                }else {
                    // console.log("No perfect match for ", eachPreff.get('make'), eachPreff.get('model'))
                }
        
            })

            
            todaysCars = getUnique(websiteStock,'uniqueness') 
            let todaysNewCars = getUnique(todaysCars,'uniqueness') 
        
            let  myNewBatch = admin.firestore().batch()
            todaysNewCars.forEach( eachCar => {
                let custReff = admin.firestore().collection('vehicles').doc()
                myNewBatch.set(custReff,eachCar)
                console.log(eachCar.carTitle)
            })
        
            await myNewBatch.commit()
            .then( resss => console.log(`Succesfully saved ${newCars.length} new cars`))
            .catch( errrr=> console.log(errrr))
        
             console.log("All cars in the feed:-",carsInTheFeed.length)
             console.log("Duplicates cars:- ",carsAlreadyExisting.length)
             console.log("New cars:-", newCars.length)
             console.log("Dolans coffee cars:-",newCars.length)

            return res.status(200).json(carsInTheFeed)
        
    })
    
};

const getDataForRun = async (req: Request, res: Response, next: NextFunction) => {

    cors( req, res, async () => {

        const content = req.body
        const apiKey = 'tt0O7QxTTa7H'

        console.log("Succesfuly received ", content.myToken, "from Google Cloud")
        // if(content.status === 'complete'){
        
        let result: AxiosResponse = await axios.get(`https://parsehub.com/api/v2/runs/${content.myToken}/data?api_key=${apiKey}&format=json`); 
        let post = result.data;
        // let carsWithImages: any[] = getObjects(post,'images','')
        let flatCars:any[] = getObjects(post,'price','')
        let carsInTheFeed: any[] = []

        console.log("Cars In Feed: ",flatCars.length)

        flatCars.forEach( eachFlatCar => {
            let hasImages: boolean = eachFlatCar.images === undefined ? false : true

            let myMileage = eachFlatCar.carLink.includes('qatarliving') ? (Math.round(getMileageKmsFromDescription(eachFlatCar.description))) : eachFlatCar.mileage.replace(/,/g, '')
            let unique: string =  eachFlatCar.carLink.includes('dubizzle') ? `${eachFlatCar.year}${eachFlatCar.make}${eachFlatCar.model}${eachFlatCar.color}${Number(eachFlatCar.cylinderCount)}${myMileage}` : eachFlatCar.carLink

            let myNewYear: number = eachFlatCar.carLink.includes('qatarliving') ? lastWord(eachFlatCar.carTitle) : eachFlatCar.carLink.includes('carsemsar') ? firstWord(eachFlatCar.carTitle) : eachFlatCar.year.replace(/[^0-9.-]+/g,"");
            let cleanTitle =  eachFlatCar.carTitle.split('Used QAR')[0]
            let moreCleanTitle = cleanTitle.replace(myNewYear,"")

            let make = eachFlatCar.carLink.includes('dubicars') ? eachFlatCar.make : eachFlatCar.carLink.includes('dubizzle') ? eachFlatCar.make : eachFlatCar.carLink.includes('motorgy') ? generateMake(eachFlatCar.carTitle) : generateMake(moreCleanTitle)
            let model = eachFlatCar.carLink.includes('dubicars') ? (eachFlatCar.model.replace(eachFlatCar.make,"").trim()) : eachFlatCar.carLink.includes('dubizzle') ? eachFlatCar.model : eachFlatCar.carLink.includes('motorgy') ? eachFlatCar.model : generateModel(make,moreCleanTitle) 
            
            let ukPrice = getUkPrice(Number(eachFlatCar.price.replace(/[^0-9.-]+/g,"")), eachFlatCar.carLink)
            let europePrice = currentEURprice 

            const basePrice =  eachFlatCar.carLink.includes('motorgy') ? (Number(eachFlatCar.price.replace(/[^0-9.-]+/g,""))): Number(eachFlatCar.price.replace(/[^0-9.-]+/g,""));

            let updatedCar = {
                price: eachFlatCar.price ? eachFlatCar.price: 0 ,
                vin:'',
                warranty: eachFlatCar.warranty? eachFlatCar.warranty:'',
                postedDate: eachFlatCar.postedDate ? eachFlatCar.postedDate:'',
                AED: basePrice ? basePrice : 0 ,
                historyCheck: eachFlatCar.historyCheck? eachFlatCar.historyCheck:'',
                GBP: ukPrice ? ukPrice : 0 ,
                EUR: europePrice ? europePrice : 0 ,
                regNumber:'',
                modelYear: Number(myNewYear) ? Number(myNewYear) : 0,
                make: make.trim().toUpperCase(),
                model: eachFlatCar.carLink.includes('motorgy') ? (eachFlatCar.carTitle.includes('Mercedes') ? generateClassesForMercedes(eachFlatCar.model): eachFlatCar.model) : model.trim().toUpperCase(),
                engineModel: eachFlatCar.carLink.includes('motorgy') ? (eachFlatCar.carTitle.includes('Mercedes') ? eachFlatCar.model: '') : (eachFlatCar.engineModel || ''),
                interiorDecor: eachFlatCar.interiorColor ? eachFlatCar.interiorColor :'',
                interiorColor: eachFlatCar.interior ? eachFlatCar.interior:'',
                regionalSpecs: eachFlatCar.regionalSpecs ? eachFlatCar.regionalSpecs:'GCC',
                transmissionType:  eachFlatCar.transmission ? eachFlatCar.transmission : 'Automatic',
                body:eachFlatCar.body? eachFlatCar.body:'',
                description: eachFlatCar.carLink.includes('oasis') ? getQatarExtras(eachFlatCar.extras): eachFlatCar.carLink.includes('dubizzle') ? (eachFlatCar.source ? (eachFlatCar.source.includes('dubizzle pro') ? descriptionForVehicle(res, basePrice) : eachFlatCar.description): eachFlatCar.description ) : (eachFlatCar.description || ""),
                bodyColor: eachFlatCar.color ? eachFlatCar.color : '',
                doors: eachFlatCar.doors ?  Number(eachFlatCar.doors.replace(/[^0-9.-]+/g,"")) : '',
                fuelType: eachFlatCar.fuelType ? eachFlatCar.fuelType : 'Petrol',
                engineCylinders: eachFlatCar.carLink.includes('dubizzle') ? Number(eachFlatCar.cylinderCount) :  eachFlatCar.carLink.includes('oasis') ? (eachFlatCar.cylinderCount.replace(/[^0-9.-]+/g,"")) : (eachFlatCar.cylinderCount ? eachFlatCar.cylinderCount.replace(/[^0-9.-]+/g,"") : ''),
                drivetrain: eachFlatCar.drivetrain ? eachFlatCar.drivetrain : '',
                engineSize: eachFlatCar.engineSize? eachFlatCar.engineSize: '',
                condition:{
                description: '',
                qualificationReport:'',
                inspectionReport:'',
                },
                isStock: false,
                // price: myPrice,
                mileageMiles: eachFlatCar.carLink.includes('qatarliving') ? (Math.round(getMileageKmsFromDescription(eachFlatCar.description)*0.621371)): Math.round((eachFlatCar.mileage.replace(/[^0-9.-]+/g,"")*0.621371)),
                // mileageMiles: eachFlatCar.mileage ? Math.round((eachFlatCar.mileage.replace(/[^0-9.-]+/g,"")*0.621371)) : 0,
                // mileage: eachFlatCar.mileage ? Number(eachFlatCar.mileage.replace(/[^0-9.-]+/g,"")):0,
                mileage: eachFlatCar.carLink.includes('qatarliving') ? getMileageKmsFromDescription(eachFlatCar.description): Number(eachFlatCar.mileage.replace(/[^0-9.-]+/g,"")),
                numberOfOwners:'',
                images: getImages(eachFlatCar),
                extras: eachFlatCar.carLink.includes('motorgy') ? (getQatarExtras(eachFlatCar.extras) || []): eachFlatCar.carLink.includes('oasis') ? [] : (eachFlatCar.extras ? eachFlatCar.extras : []),
                customExtras: eachFlatCar.customExtras  ? eachFlatCar.customExtras : [],
                carTitle: eachFlatCar.source ? (eachFlatCar.source.includes('dubizzle cars') ? carTitleWithMoreDetails(res) : cleanTitle) : cleanTitle,
                dubizzleTitle: eachFlatCar.carLink.includes('dubizzle') ? eachFlatCar.carTitle : eachFlatCar.carLink.includes('qatarliving') ? cleanTitle: eachFlatCar.carTitle,
                carLink: eachFlatCar.carLink,
            // let  =  eachFlatCar.carTitle.split('Used QAR')[0]
        
                userId:'HqYwd3wisRbztV9llAQebQOTf0e2',
                owner: 'Elkana Rop',
                ownerCategory: 'individual',
                ownerCompany: 'individual',
                ownerPhone:'',
                status:'published',
                master:true,
                favourite: false,
                tag: eachFlatCar.source? eachFlatCar.source:'',
                // source: eachFlatCar.carLink.includes("qmotor") ? 'qmotors': eachFlatCar.carLink.includes("dubicars") ? 'dubicars': eachFlatCar.carLink.includes("qatarliving") ? 'qliving' : eachFlatCar.carLink.includes("oasiscars") ? 'oasis' : eachFlatCar.carLink.includes("qatarsale") ? 'qatarsale': eachFlatCar.carLink.includes("carsemsar") ? 'carsemsar': eachFlatCar.carLink.includes("motorgy") ? 'motorgy' : 'dubizzle',
                source: eachFlatCar.carLink.includes("dubicars") ? 'dubicars':eachFlatCar.carLink.includes("qmotor") ? 'qmotors': eachFlatCar.carLink.includes("dubicars") ? 'dubicars': eachFlatCar.carLink.includes("qatarliving") ? 'qatarliving' : eachFlatCar.carLink.includes("oasiscars") ? 'oasis' : eachFlatCar.carLink.includes("qatarsale") ? 'qatarsale': eachFlatCar.carLink.includes("carsemsar") ? 'carsemsar': eachFlatCar.carLink.includes("motorgy") ? 'motorgy' : eachFlatCar.carLink.includes("q84sale") ? 'q84sale': eachFlatCar.carLink.includes("dubizzle") ? (eachFlatCar.source.includes("wner")? 'dubizzle': eachFlatCar.source.includes("dubizzle cars")? 'dubizzle pro': 'dubizzle') : 'dubizzle',
                version:2,
                depositeTaken: eachFlatCar.carTitle.toLowerCase().includes('taken') ? true:false,
                horsePower: eachFlatCar.horsePower? eachFlatCar.horsePower: '',
                dateStamp: Date.parse(new Date().toUTCString()),
                stockNumber: randNumber(),
                createdDate: Date.parse(formatDate(new Date().toUTCString())),
                qualificationToken: {
                approved: false,
                approvedBy: 'HqYwd3wisRbztV9llAQebQOTf0e2',
                dateTimeApproved: Date.parse(new Date().toUTCString()),
        
                },
                // uniqueness:`${Number(eachFlatCar.year)}${eachFlatCar.model.trim().toLowerCase()}${Number(eachFlatCar.cylinderCount)}${Number(eachFlatCar.mileage)}`,
                uniqueness: eachFlatCar.carLink.includes('dubizzle') ? unique : eachFlatCar.carLink,
                market: eachFlatCar.carLink.includes('dubizzle') ? 'dubai' : eachFlatCar.carLink.includes('dubicars') ? 'dubai' : 'emirates',
                hasImages: hasImages

            }


            if(updatedCar.hasImages){

                // console.log(`${updatedCar.carTitle} has IMAGES`)
                carsInTheFeed.push(updatedCar)
                checkIfExists(updatedCar)

            }else{

                // console.log(`Skipt ${updatedCar.carTitle} with no IMAGES!`)
            }

            
        })
        
        let todaysCars: any[] = []
        adminPress.forEach(  eachPreff => {

            let myResults: any[] = newCars.filter( car=> car.make.replace(/[^a-zA-Z ]/g, "").replace(/\s/g, "").toLowerCase().trim() === eachPreff.get('make').replace(/[^a-zA-Z ]/g, "").replace(/\s/g, "").toLowerCase().trim() && 
            car.carTitle.replace(/[^a-zA-Z ]/g, "").replace(/\s/g, "").toLowerCase().trim().includes(eachPreff.get('model').replace(/[^a-zA-Z ]/g, "").replace(/\s/g, "").toLowerCase().trim()) )

            if(myResults.length>0){
                websiteStock.push(...myResults)
                // console.log("Perfect match for ", eachPreff.get('make'), eachPreff.get('model'))
            }else {
                // console.log("No perfect match for ", eachPreff.get('make'), eachPreff.get('model'))
            }

        })

        
        todaysCars = getUnique(websiteStock,'uniqueness') 
        let todaysNewCars = getUnique(todaysCars,'uniqueness') 

        let  myNewBatch = admin.firestore().batch()
        todaysNewCars.forEach( eachCar => {
            let custReff = admin.firestore().collection('vehicles').doc()
            myNewBatch.set(custReff,eachCar)
            console.log(eachCar.carTitle)
        })

        await myNewBatch.commit()
        .then( resss => console.log(`Succesfully saved ${newCars.length} new cars`))
        .catch( errrr=> console.log(errrr))

        console.log("All cars in the feed:-",carsInTheFeed.length)
        console.log("Duplicates cars:- ",carsAlreadyExisting.length)
        console.log("New cars:-", newCars.length)
        console.log("Dolans coffee cars:-",newCars.length)

        return res.status(200).json(carsInTheFeed)

        // } else console.log('Loading GetDataForRun...')

    })

        
};

//// GET OBJECTS WITH MAKE FROM JSON FILE ////////
   function getObjects(obj: any, key: string, val: string) {
    //  console.log("RECEIVED", obj.length )
    let objects: any[] = [];
    for (var i in obj) {
        if (!obj.hasOwnProperty(i)) continue;
        if (typeof obj[i] == 'object') {
            objects = objects.concat(getObjects(obj[i], key, val));    
        } else 
        //if key matches and value matches or if key matches and value is not passed (eliminating the case where key matches but passed value does not)
        if (i == key && obj[i] == val || i == key && val == '') { //
            objects.push(obj);
        } else if (obj[i] == val && key == ''){
            //only add if the object is not already in the array
            if (objects.lastIndexOf(obj) == -1){
                objects.push(obj);
            }
        }
    }
    return objects;
  }

  function batchIncomingCars(car: any) {

    

    admin.firestore().collection('stock').doc().set(car)
    .then(doc => console.log("Message Saved"))
    .catch( error =>  console.log("Error adding Eng ", error))
  
    }


 function fetchAllPrefferences() {
    // return  admin.firestore().collection('carPreffs')
    return  admin.firestore().collection('carPreffs').get()
 }

 function allAvailableCars() {
    // return  admin.firestore().collection('carPreffs')
    return  admin.firestore().collection('vehicles').get()
 }

function generateMake(carTitle: string){
    let myPref: any = adminPress.docs.find( preff => { carTitle.replace(/[^a-zA-Z ]/g, "").replace(/\s/g, "").toLowerCase().includes(preff.get('make').replace(/[^a-zA-Z ]/g, "").replace(/\s/g, "").toLowerCase())})
    return myPref ? myPref.make : carTitle.replace(/ .*/,'')
}

function generateModel(carMake: string, cleanTitle: string){
    let myPref: any = adminPress.docs.find( preff => preff.get('make').replace(/[^a-zA-Z ]/g, "").replace(/\s/g, "").toLowerCase().trim() === carMake.replace(/[^a-zA-Z ]/g, "").replace(/\s/g, "").toLowerCase().trim() && cleanTitle.replace(/[^a-zA-Z ]/g, "").replace(/\s/g, "").toLowerCase().includes(preff.get('model').replace(/[^a-zA-Z ]/g, "").replace(/\s/g, "").toLowerCase()))
    return myPref ? myPref.model.trim(): cleanTitle.toLowerCase().replace(carMake.toLowerCase().trim(),'')
} 
  
  
function lastWord(carTitle: string) {
    let year = carTitle.split(" ");
    let myYear = year[year.length - 1]
    console.log("Last Word: ", myYear.replace(/[^0-9.-]+/g,""))
    return myYear.replace(/[^0-9.-]+/g,"")

}
  
function firstWord(carTitle: string) {
    let title  = carTitle.trim()
    let myTitle: any[] = title.split(" ")
    return myTitle[0].replace(/[^0-9.-]+/g,"")  
}



function getQatarExtras(car: any){

    let myExtras: any[] = car
    let combined:any[] = []

    if(myExtras.length>0){
      myExtras.forEach( (res: any) => { combined.push(res.extra)})
      return combined.join()
    }
   
}


function generateClassesForMercedes(model: string){
    let mercedesClass = model.replace(model.replace(/[^0-9.-]+/g,""),"").trim()
    return `${mercedesClass}-CLASS`  
}

function descriptionForVehicle(carDetails: any, price: any){
    let descrip =  carDetails.description.substring(carDetails.description.indexOf(carDetails.modelYear))
    let newPrice = addCommasToPrice(price.toString())
    let cleanDescrp =  descrip.split(newPrice)[0]
    return cleanDescrp.replace(newPrice,"").replace("AED","").replace(/-/g, "")
}

function addCommasToPrice(nStr: any) {
    nStr += '';
    var x = nStr.split('.');
    var x1 = x[0];
    var x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}


  function carTitleWithMoreDetails(carDetails: any){
    let descrip =  carDetails.description.substring(carDetails.description.indexOf(carDetails.year))
    return descrip.split('BHP')[0]+' BHP'
  }

  function getMileageKmsFromDescription(description: string): number{
    let myDesc: string = description.trim()
    if(myDesc.includes('0 mileage')){
    console.log("ZERO MILEAGE")
    return 0;
    }else {
    let allWordsBeforeKm = myDesc.substring(0, myDesc.toLowerCase().indexOf(" km "));
    let isolateWords = allWordsBeforeKm.split(" ");
    let mileageKms = isolateWords[isolateWords.length - 1]
    return Number(mileageKms.replace(/[^0-9.-]+/g,""))

    }
   
  }


  function getUkPrice(price: number, carLink: string): number{
    // console.log("Received Price", price)
    let margin: number = 0;
    let shipping: number = 3400;
    let oasisAdminFee: number = 0

    if(carLink.includes('dubi')){
        channel = `Dubai`
    }else if(carLink.includes('motorg')){
        channel = `Kuwait`
    }else {
        channel = `Kuwait`
    }
    let my30pcDuties: number = 0
    let prepWork: number = 0
    let registrationFee: number = 1000
    let unitedKingdomPrice: number = 0
  
    if (price <= 200000) {
        margin = 4000
    } else if(price > 200000){
       margin = 4500
    }  else if(price > 200000){
      margin = 4500
    } 
  
    if(channel === 'Dubai'){
     my30pcDuties = (duties/100)*(price*uaeDirhamToGbpRate)
     unitedKingdomPrice = price*uaeDirhamToGbpRate
  
    }else if(channel === 'Qatar'){
     my30pcDuties = (duties/100)*(price*qatariRialToGbpRate)
     prepWork = 250
     oasisAdminFee = 1000
     unitedKingdomPrice = price*qatariRialToGbpRate
  
    }else {
     my30pcDuties = (duties/100)*(price*kuweitDinnarToGbpRate)
     prepWork = 600
     unitedKingdomPrice = price*kuweitDinnarToGbpRate
    }
  
    let finalUkPrice = unitedKingdomPrice + oasisAdminFee+ prepWork + margin + shipping  + my30pcDuties + registrationFee
  
    currentEURprice = finalUkPrice*gbpToEurRate

    // console.log("Produced Price", finalUkPrice)
  
    return finalUkPrice
  
  }


function randNumber(){
    return Math.floor(Math.random()*90000) + 10000;
   } 
   
function dateToday() {
     return  new Date()
   }
 
function getUnique(cars: any[], column: string) {

    // store the comparison  values in array
  const unique =  cars.map(e => e[column])
  // store the indexes of the unique objects
  .map((e, i, final) => final.indexOf(e) === i && i)
  // eliminate the false indexes & return unique objects
  .filter((e:any) => cars[e]).map((e:any) => cars[e]);
  return unique;
  
  }


  function formatDate(date: any) {

    // console.log("Received Date: ", date)
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) 
        month = '0' + month;
    if (day.length < 2) 
        day = '0' + day;

        // console.log("Returned Date: ", [year, month, day].join('-'))

    return [year, month, day].join('-');
}


// function checkIfExists(car: any): boolean{
   
//     let token:boolean = false
//     let itExists = stock.docs.filter(myCar => myCar.get('uniqueness') === car.uniqueness);
//     if(itExists.length===0){
//         console.log(`${car.make} ${car.model} is New`)
//       newCars.push(car)
//       return token = false
//     }else {
//       console.log(`${car.make} ${car.model} is Old`)
//       carsAlreadyExisting.push(car)
//       return token = true
//     }

//   }

function checkIfExists(car: any){

    let results = stock.docs.filter(myCar => myCar.get('uniqueness') === car.uniqueness);
    if(results.length<1) newCars.push(car)
    else carsAlreadyExisting.push(car)
}

  function getImages(car: any){

    if(car.images){

        let images:any[] = []
        let gallery = []

        gallery = car.images 
        gallery.forEach( (eachImage: any) => {
            const picture = Object.create(eachImage)
            images.push(picture.image)
         });

    return images

    }return []
    

  }
  


export default { getProjects, getLastReadyData, getDataForRun };