/** source/routes/posts.ts */
import express from 'express';
import controller from '../controllers/parsehub';
const router = express.Router();
  
router.get('/projects/:apiKey/:projectToken', controller.getProjects);
// router.get('/last-ready-data/:apiKey/:projectToken', controller.getLastReadyData);
router.get('/last-ready-data/:apiKey/:projectToken', controller.getLastReadyData);
router.post('/data-for-run/:apiKey/:runToken', controller.getDataForRun);


export = router;